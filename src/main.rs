use tokio::runtime::Runtime;
use serde::{Serialize, Deserialize};
use structopt::StructOpt;

// Cli flags
#[derive(StructOpt)]
struct CliArgs{
    /// Remove existing API keys
    #[structopt(short, long)]
    remove: bool,
}

// Config file layout
#[derive(Debug, Serialize, Deserialize)]
struct Config {
    coinlayer_key: String,
}

/// Generate default config file
impl ::std::default::Default for Config {
    fn default() -> Self {
        Self {
            coinlayer_key: String::from(""),
        }
    }
}

// Execute certain functions based on flags
fn flag_operations(args: CliArgs) {
    if args.remove == true {
        let blank_config = Config {
            coinlayer_key: String::from(""),
        };
        confy::store("crypt", blank_config).expect("Could not remove previous keys");
    }
}

fn main() -> Result<(), confy::ConfyError> {
    flag_operations(CliArgs::from_args());
    let config: Config = confy::load("crypt")?;

    // Prompt user to enter API keys
    if config.coinlayer_key == "" {
        println!("Enter your Coinlayer API key: ");
        let coin_key = crypt::input_coin_key();

        println!("Enter your Exchange Rates API key: ");
        let mut exch_key = String::new();
        let _exch_input = std::io::stdin().read_line(&mut exch_key).unwrap();
        let updated_config = Config {
            coinlayer_key: coin_key,
        };
        confy::store("crypt", updated_config)?;
    }
    let config: Config = confy::load("crypt")?;

    let crypto_values = Runtime::new().expect("Could not collect crypto values")
        .block_on(crypt::collect_values(&config.coinlayer_key))
        .unwrap();

    let exchange_rates = Runtime::new().expect("Could not collect exchange rates")
        .block_on(crypt::collect_exchanges())
        .unwrap();

    //println!("{}", crypto_values);
    println!("{}", exchange_rates);

    Ok(())
}
