use std::collections::HashMap;
use reqwest::Client;
use serde_json::{Result, Value};

// Input key
pub fn input_coin_key() -> std::string::String {
    let mut coin_key = String::new();
    let _coin_input = std::io::stdin().read_line(&mut coin_key).unwrap();
    return coin_key;
}

// Validate key
pub async fn validate_coin_key(key: &String) -> Result<std::string::String> {
    let url = &format!("http://api.coinlayer.com/live?access_key={}", key);
    let res = Client::new()
        .get(url)
        .send()
        .await.expect("Could not send request")
        .text()
        .await.expect("Could not convert response to text");

    let res_output: Value = serde_json::from_str(&res).expect("Could not convert response to JSON");
    let success = res_output["success"].to_string();

    if success != "true" {
        println!("Inputted key is invalid");
        input_coin_key();
    }

    Ok(success)
}

// Collect information
pub async fn collect_values(key: &String) -> Result<std::collections::HashMap<&str, &str>>{
    let url = &format!("http://api.coinlayer.com/live?access_key={}", key);
    let res = Client::new()
        .get(url)
        .send()
        .await.expect("Could not send request")
        .text()
        .await.expect("Could not convert response to text");

    let res_output: Value = serde_json::from_str(&res).expect("Could not convert response to JSON");
    let rates = res_output["rates"].to_string();
    let mut stripped_rates = String::new();

    for c in rates.chars() {
        if !"{}\"".contains(c) {
            stripped_rates.push(c);
        }
    }
    let rates_vec: Vec<&str> = stripped_rates.split(",").collect();

    let mut hash_rates = HashMap::new();
    for element in rates_vec {
        let pair: Vec<&str> = element.split(":").collect();
        hash_rates.insert(pair[0], pair[1]);
    }

    // This works, I just can't get it outputting properly

    Ok(hash_rates)
}

pub async fn collect_exchanges() -> Result<std::string::String> {
    let url = "https://api.exchangeratesapi.io/latest?base=USD";
    let res = Client::new()
        .get(url)
        .send()
        .await.expect("Could not send request")
        .text()
        .await.expect("Could not convert response to text");

    let res_output: Value = serde_json::from_str(&res).expect("Could not convert response to JSON");
    let res_string = res_output.to_string();

    Ok(res_string)
}
